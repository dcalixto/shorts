# Great Management Reads

## Posts

- [Radical Candor](http://firstround.com/review/radical-candor-the-surprising-secret-to-being-a-good-boss/)
- [101 Questions to Ask in One on Ones](http://jasonevanish.com/2014/05/29/101-questions-to-ask-in-1-on-1s/)
- [Engineering Management](http://algeri-wong.com/yishan/engineering-management-process.html) - engineering specific

## Books

- [High Output Management](http://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884)
