# Shorts

Short, random tutorials

- [Learn Data Science](Learn-Data-Science.md)
- [PgBouncer Setup](PgBouncer-Setup.md)
- [Development Rails](Development-Rails.md)
- [Adding CSP to Rails](CSP-Rails.md)
- [Security Checks](Security-Checks.md)
- [Data Science SQL](Data-Science-SQL.md)
- [Rails on Heroku](Rails-on-Heroku.md)
- [The Origin of SQL Queries](The-Origin-of-SQL-Queries.md)
- [R and Database URLs](R-Postgres-and-Database-URLs.md)
- [Dokku on DigitalOcean](Dokku-Digital-Ocean.md)
- [Host Your Own Postgres](Host-Your-Own-Postgres.md)
- [The Two Metrics You Need](Two-Metrics.md)
- [Zero Downtime Migrations with Postgres](Zero-Downtime-Migrations.md)
- [Scaling Reads](Scaling-Reads.md)
- [attr_accessible to Strong Parameters](Strong-Parameters.md)
- [Instrumenting Caches](Instrumenting-Caches.md)

Short, random posts

- [Favorite Quotes](Favorite-Quotes.md)
- [Great Leadership Reads](Leadership-Reads.md)
- [Great Management Reads](Management-Reads.md)
- [Great Programming Reads](Programming-Reads.md)
- [Distributed Architecture Talks](Distributed-Architecture-Talks.md)

Guides

- [The Ultimate Guide to Ruby Timeouts](https://github.com/ankane/the-ultimate-guide-to-ruby-timeouts)
- [Secure Rails](https://github.com/ankane/secure_rails)
- [Production Rails](https://github.com/ankane/production_rails)
- [Search Guide](https://github.com/ankane/search_guide)
- [Ahoy Guide](https://github.com/ankane/ahoy_guide)

Latest projects

- [pgsync](https://github.com/ankane/pgsync) - quickly and securely sync data between environments
- [Strong Migrations](https://github.com/ankane/strong_migrations) - catch unsafe migrations at dev time
- [Jetpack](https://github.com/ankane/jetpack) - simple package management for R
- [g index](https://github.com/ankane/gindex) - concurrent index migrations for Rails
